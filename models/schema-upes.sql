INSERT INTO ACAU_EstadoCivil (idEstadoCivil, nombre, usuarioIngreso, fechaIngreso, 
	usuarioActualiza, fechaActualiza, EstadoCivil) VALUES
    (0001, 'Soltero', '@proto', '2017-11-30', '@proto', '2017-11-30', ''),
    (0002, 'Casado', '@proto', '2017-11-30', '@proto', '2017-11-30', ''),
    (0003, 'Divorciado', '@proto', '2017-11-30', '@proto', '2017-11-30', '');

INSERT INTO ACAU_EstatusSolicitud (idEstatusSolicitud, nombreEstatusSolicitud, usuarioIngreso, 
	fechaIngreso, usuarioActualiza, fechaActualiza) VALUES
    (0001, 'Proceso', '@proto', '2017-11-29', '@proto', '2017-11-30' ),
    (0002, 'Aprobado', '@proto', '2017-11-29', '@proto', '2017-11-30' );

INSERT INTO ACAU_Nacionalidad (idNacionalidad, nombre, usuarioIngreso, fechaIngreso,
    usuarioActualiza, fechaActualiza) VALUES
    (0001, 'Salvadoreno', '@proto', '2017-11-30', '@proto', '2017-11-30'),
    (0002, 'Guatemalteco', '@proto', '2017-11-30', '@proto', '2017-11-30'),
    (0003, 'Hondureno', '@proto', '2017-11-30', '@proto', '2017-11-30');

INSERT INTO ACAU_TipoIngreso (idTipoIngreso, nombre, usuarioIngreso, fechaIngreso, 
	usuarioActualiza, fechaActualiza) VALUES
    (0001, 'Nuevo ingreso', '@proto', '2017-11-29', '@proto', '2017-11-30' ),
    (0002, 'Antiguo ingreso', '@proto', '2017-11-29', '@proto', '2017-11-30' ),
    (0003, 'Equivalencias', '@proto', '2017-11-29', '@proto', '2017-11-30' );

INSERT INTO GRAU_Cantones (idCanton, nombreCanton, idMunicipio, usuarioIngreso, 
    fechaIngreso, usuarioActualiza, fechaActualiza) VALUES
    (4000, 'Las Palmas', 3000, '@proto', '2017-11-29', '@proto', '2017-11-30' ),
    (4001, 'El Cerro', 3000, '@proto', '2017-11-29', '@proto', '2017-11-30' );

INSERT INTO GRAU_Municipios (idMunicipio, nombreMunicipio, idDepartamento, usuarioIngreso,
    fechaIngreso, usuarioActualiza, fechaActualiza) VALUES
    (3000, 'San Salvador', 2000, '@proto', '2017-11-29', '@proto', '2017-11-30' ),
    (3001, 'Olocuilta', 2000, '@proto', '2017-11-29', '@proto', '2017-11-30' );

INSERT INTO GRAU_Departamentos (idDepartamento, nombreDepartamento, idPaises, usuarioIngreso,
    fechaIngreso, usuarioActualiza, fechaActualiza ) VALUES
    (2000, 'San Salvador', 1000, '@proto', '2017-11-29', '@proto', '2017-11-30' ),
    (2001, 'La Paz', 1000, '@proto', '2017-11-29', '@proto', '2017-11-30' );

INSERT INTO GRAU_Paises (idPaises, nombrePais, usuarioIngreso, fechaIngreso,
    usuarioActualiza, fechaActualiza) VALUES
    (1000, 'El Salvador', '@proto', '2017-11-29', '@proto', '2017-11-30' ),
    (1001, 'Guatemala', '@proto', '2017-11-29', '@proto', '2017-11-30' );

INSERT INTO ACAU_Solicitudes (idSolicitud, fechaSolicitud, nombres, apellidoUno, apellidoDos, apellidoCasada, fechaNacimiento,
    genero, dui, nit, carnetMinoridad, idNacionalidad, idPaisesNacimiento, direccion, idPaisesDireccion,
    idDepartamentoDireccion, idMunicipioDireccion, idCantonDireccion, telefono, correo, correoDos, idEstadoCivil,
    idTipoIngreso, idEstatusSolicitud, usuarioIngreso, fechaIngreso, usuarioActualiza, fechaActualiza) VALUES
    (8000, '2017-11-29', 'Paul Ernest', 'Saint', 'Lopez', '', '2017-11-29', 'Masculino', '00895743-7', 
    '0890-201012-101-2', '', 0001, 1000, '6ta calle oriente', 1000, 2001, 3001, 4001, '2330-6853',
    'jnew@gmail.com', '', 0001, 0002, 0001, '@proto', '2017-11-29', '@proto', '2017-11-30'),
    (8000, '2017-11-29', 'Bilbo', 'Bolson', 'Cruz', '', '2017-11-29', 'Masculino', '00895790-5', 
    '0890-200231-101-1', '', 0001, 1000, 'Barrio La Cruz', 1000, 2001, 3001, 4001, '2330-6820',
    'bbolson@gmail.com', '', 0001, 0002, 0001, '@proto', '2017-11-29', '@proto', '2017-11-30');

INSERT INTO ACAU_Requisitos (idACAU_Requisitos, descripcion, estado, usuarioIngreso, fechaIngreso,
    usuarioActualiza, fechaActualiza) VALUES
    (1000, 'dui', 0, '@proto', '2017-11-29', '@proto', '2017-11-30' ),
    (1001, 'nit', 0, '@proto', '2017-11-29', '@proto', '2017-11-30' );

INSERT INTO ACAU_requisitossolicitud (idRequisitosSolicitud, idRequisitos, dTipoIngreso, usuarioIngreso, fechaIngreso,
    usuarioActualiza, fechaActualiza) VALUES
    (2000, 1000, 1, '@proto', '2017-11-29', '@proto', '2017-11-30' ),
    (2001, 1001, 1, '@proto', '2017-11-29', '@proto', '2017-11-30' );