<?php
class RegistroModel extends Model{
	public function set( $so_data = array()){
		foreach ($so_data as $key => $value) {
        	$$key = $value;
        }

        $this->query = "REPLACE INTO ACAU_Solicitudes SET idSolicitud = '$idSolicitud', fechaSolicitud = '$fechaSolicitud', nombres = '$nombres', apellidoUno = '$apellidoUno', apellidoDos = '$apellidoDos', apellidoCasada ='$apellidoCasada', fechaNacimiento = '$fechaNacimiento', genero = '$genero', dui ='$dui', nit ='$nit', carnetMinoridad = '$carnetMinoridad', idNacionalidad = '$idNacionalidad', idPaisesNacimiento ='$idPaisesNacimiento', direccion = '$direccion', idPaisesDireccion = '$idPaisesDireccion', idDepartamentoDireccion = '$idDepartamentoDireccion', idMunicipioDireccion = '$idMunicipioDireccion', idCantonDireccion = '$idCantonDireccion', telefono = '$telefono', correo = '$correo', correoDos = '$correoDos', idEstadoCivil = '$idEstadoCivil', idTipoIngreso = '$idTipoIngreso', idEstatusSolicitud = '$idEstatusSolicitud', usuarioIngreso = '$usuarioIngreso', fechaIngreso = '$fechaIngreso', usuarioActualiza = '$usuarioActualiza', fechaActualiza = '$fechaActualiza'";

        $this->set_query();
	}

	public function get( $so = '' ){
		$this->query = ($so != '')
		   ?"SELECT so.idSolicitud, so.fechaSolicitud, so.nombres, so.apellidoUno, so.apellidoDos, so.apellidoCasada, so.fechaNacimiento, so.genero, so.dui, so.nit, so.carnetMinoridad, so.idNacionalidad, so.idPaisesNacimiento, so.direccion, so.idPaisesDireccion, so.idDepartamentoDireccion, so.idMunicipioDireccion, so.idCantonDireccion, so.telefono, so.correo, so.correoDos, so.idEstadoCivil, so.idTipoIngreso, so.idEstatusSolicitud, so.usuarioIngreso, so.fechaIngreso, so.usuarioActualiza, so.fechaActualiza
            FROM ACAU_Solicitudes AS so INNER JOIN ACAU_Nacionalidad AS s ON so.idNacionalidad = s.idNacionalidad
            WHERE so.idSolicitud = '$so'"
		   :"SELECT so.idSolicitud, so.fechaSolicitud, so.nombres, so.apellidoUno, so.apellidoDos, so.apellidoCasada, so.fechaNacimiento, so.genero, so.dui, so.nit, so.carnetMinoridad, so.idNacionalidad, so.idPaisesNacimiento, so.direccion, so.idPaisesDireccion, so.idDepartamentoDireccion, so.idMunicipioDireccion, so.idCantonDireccion, so.telefono, so.correo, so.correoDos, so.idEstadoCivil, so.idTipoIngreso, so.idEstatusSolicitud, so.usuarioIngreso, so.fechaIngreso, so.usuarioActualiza, so.fechaActualiza
            FROM ACAU_Solicitudes AS so INNER JOIN ACAU_Nacionalidad AS s ON so.idNacionalidad = s.idNacionalidad";
  
        $this->get_query();
                
        $num_rows = count($this->rows);
        
        $data = array ();

        foreach ($this->rows as $key => $value) {
        	array_push($data, $value);
        }

        return $data;
    }    

	public function del( $so = ''){
		$this->query = "DELETE FROM ACAU_Solicitudes WHERE idSolicitud = '$so'";
        $this->set_query();
	}
   
	public function __destruct(){
		//unset($this);
	}
}