<?php
class DeptoController{
	private $model;

	public function __construct() {
		$this->model = new DeptoModel();
	}

    public function set( $dp_data = array()){
    	return $this->model->set($dp_data);
    }

    public function get( $id_depto = '' ){
       return $this->model->get($id_depto);
    }

    public function del( $id_depto = ''){
    	return $this->model->del($id_depto);
    }

    public function __destruct(){
    	//unset($this);
    }
}