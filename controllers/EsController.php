<?php
class EsController{
	private $model;

	public function __construct() {
		$this->model = new EsModel();
	}

    public function set( $es_data = array()){
    	return $this->model->set($es_data);
    }

    public function get( $id_es = '' ){
       return $this->model->get($id_es);
    }

    public function del( $id_es = ''){
    	return $this->model->del($id_es);
    }

    public function __destruct(){
    	//unset($this);
    }
}