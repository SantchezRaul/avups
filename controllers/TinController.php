<?php
class TinController{
	private $model;

	public function __construct() {
		$this->model = new TinModel();
	}

    public function set( $ti_data = array()){
    	return $this->model->set($ti_data);
    }

    public function get( $id_ti = '' ){
       return $this->model->get($id_ti);
    }

    public function del( $id_ti = ''){
    	return $this->model->del($id_ti);
    }

    public function __destruct(){
    	//unset($this);
    }
}