<?php
class RegistroController{
	private $model;

	public function __construct() {
		$this->model = new RegistroModel();
	}

    public function set( $so_data = array()){
    	return $this->model->set($so_data);
    }

    public function get( $so = '' ){
       return $this->model->get($so);
    }

    public function del( $so = ''){
    	return $this->model->del($so);
    }

    public function __destruct(){
    	//unset($this);
    }
}