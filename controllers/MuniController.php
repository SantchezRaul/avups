<?php
class MuniController{
	private $model;

	public function __construct() {
		$this->model = new MuniModel();
	}

    public function set( $mu_data = array()){
    	return $this->model->set($mu_data);
    }

    public function get( $id_muni = '' ){
       return $this->model->get($id_muni);
    }

    public function del( $id_muni = ''){
    	return $this->model->del($id_muni);
    }

    public function __destruct(){
    	//unset($this);
    }
}