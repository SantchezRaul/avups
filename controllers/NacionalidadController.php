<?php
class NacionalidadController{
	private $model;

	public function __construct() {
		$this->model = new NacionalidadModel();
	}

    public function set( $nacio_data = array()){
    	return $this->model->set($nacio_data);
    }

    public function get( $id_nacio = '' ){
       return $this->model->get($id_nacio);
    }

    public function del( $id_nacio = ''){
    	return $this->model->del($id_nacio);
    }

    public function __destruct(){
    	//unset($this);
    }
}