<?php
class PaisController{
	private $model;

	public function __construct() {
		$this->model = new PaisModel();
	}

    public function set( $ps_data = array()){
    	return $this->model->set($ps_data);
    }

    public function get( $id_pais = '' ){
       return $this->model->get($id_pais);
    }

    public function del( $id_pais = ''){
    	return $this->model->del($id_pais);
    }

    public function __destruct(){
    	//unset($this);
    }
}