<?php
class EsciController{
	private $model;

	public function __construct() {
		$this->model = new EsciModel();
	}

    public function set( $ec_data = array()){
    	return $this->model->set($ec_data);
    }

    public function get( $id_esci = '' ){
       return $this->model->get($id_esci);
    }

    public function del( $id_esci = ''){
    	return $this->model->del($id_esci);
    }

    public function __destruct(){
    	//unset($this);
    }
}