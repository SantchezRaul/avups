<?php
class Router{
	public $route;
	public function __construct($route) {
		//http://php.net/manual/es/function.session-star.php
		//http://php.net/manual/es/session.configuration.php
		//buscar opciones en el PHP.INI
		/*$session_options = array(
			'use_only_cookies' => 1, 
			'auto_start' => 1,
			'read_and_close' => true
		);*/
		if ( !isset($_SESSION) ) session_start(); 

		if ( !isset($_SESSION['ok']) ) $_SESSION['ok'] = false;
		
		if($_SESSION['ok']) {
			//Aqui va toda la programación de nuesra webapp
			//echo 'Debe cargar el archivo home';
			$this->route = isset($_GET['r']) ? $_GET['r'] : 'home'; //34

			$controller = new ViewController(); //33
			
			switch ($this->route) { //34
				case 'home':
					$controller->load_view('home'); //33
					break;

				case 'usuarios':
				    if ( !isset( $_POST['r']) ) $controller->load_view('users'); 
				    else if ($_POST['r'] == 'users-add' ) $controller->load_view('users-add');
				    else if ($_POST['r'] == 'users-edit' ) $controller->load_view('users-edit');
				    else if ($_POST['r'] == 'users-delete' ) $controller->load_view('users-delete');	    
					break;

				case 'registro':
				    if ( !isset( $_POST['r']) ) $controller->load_view('registro'); 
				    else if ($_POST['r'] == 'registro-add' ) $controller->load_view('registro-add');
				    else if ($_POST['r'] == 'registro-edit' ) $controller->load_view('registro-edit');
				    else if ($_POST['r'] == 'registro-delete' ) $controller->load_view('registro-delete');
				    else if ($_POST['r'] == 'registro-show' ) $controller->load_view('registro-show');
				   	break;

				case 'salir':
					$user_session = new SessionController();
					$user_session->logout(); 
					break;				

				default:
					$controller->load_view('error404'); 
					break;
			}
			
		} else{
			if ( !isset($_POST['user']) && !isset($_POST['pass']) ) {
				//mostrar un formulario de autenticación
			    $login_form = new ViewController();
			    $login_form->load_view('login');
			}else{
				//echo "Estamos aca";
				$user_session = new SessionController();
				$session = $user_session->login($_POST['user'], $_POST['pass']);

				if ( empty($session) ) {
				    //echo 'El usuario y contrasena son incorrectos';
				    $login_form = new ViewController();
			        $login_form->load_view('login');

                    header('Location: ./?error=El usuario ' .$_POST['user'].
                        ' y el password proporcionado no coinciden');

			    } else {
				    //echo 'El usuario y contrasena son correctos';
				    //var_dump($session);
				    $_SESSION['ok'] = true;

				    foreach ($session as $row) {
				    	$_SESSION['user'] = $row['user'];
				    	$_SESSION['email'] = $row['email'];
				    	$_SESSION['name'] = $row['name'];
				    	$_SESSION['birthday'] = $row['birthday'];
				    	$_SESSION['pass'] = $row['pass'];
				    	$_SESSION['role'] = $row['role'];
				    }

				    header('Location: ./');
			    }
			}		
		}
	}
	
	public function __destruct(){
    	//unset($this);
    }
}