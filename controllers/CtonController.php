<?php
class CtonController{
	private $model;

	public function __construct() {
		$this->model = new CtonModel();
	}

    public function set( $cn_data = array()){
    	return $this->model->set($cn_data);
    }

    public function get( $id_cton = '' ){
       return $this->model->get($id_cton);
    }

    public function del( $id_cton = ''){
    	return $this->model->del($id_cton);
    }

    public function __destruct(){
    	//unset($this);
    }
}