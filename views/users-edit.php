<?php
//echo '<h2>Editar</h2>';
$users_controller = new UsersController();
if ($_POST['r'] == 'users-edit' && $_SESSION['role'] == 'Admin' && !isset($_POST['crud']) ) {
	$users = $users_controller->get($_POST['user']);

	if ( empty($users) ) {
		$template ='
		    <div class="container">
		        <p class="item error">No existe el user <b>%s</b></p>
		    </div>
		    <script>
	            window.onload = function (){
	        	    reloadPage("usuarios")
	            }
	        </script>    
		';

		printf($template, $_POST['user']);
	} else {
		$role_admin = ($users[0]['role'] == 'Admin') ? 'checked' : '';
		$role_user = ($users[0]['role'] == 'User') ? 'checked' : '';
		$template_users ='
		    <h2 class="p1">Editar Usuarios</h2>
		    <form method="POST" class="item">
	            <div class="p_25">
	                <input type="text" placeholder="usuario" value="%s" disabled required>
	                <input type="hidden" name="user" value="%s">
    	        </div>
    	        <div class="p_25">
    	            <input type="email" name="email" placeholder="email" value="%s" required>
    	        </div>
    	        <div class="p_25">
    	            <input type="text" name="name" placeholder="name" value="%s" required>
    	        </div>
    	        <div class="p_25">
    	            <input type="text" name="birthday" placeholder="cumpleanos" value="%s" required>
    	        </div>
    	        <div class="p_25">
    	            <input type="password" name="pass" placeholder="password" value="" required>
    	        </div>
    	        <div class="p_25">
    	            <input type="radio" name="role" id="admin" value="Admin" %s required><label for="admin">Administrador</label>
    	            <input type="radio" name="role" id="noadmin" value="User" %s required><label for="noadmin">Usuario</label>
    	        </div>
	            <div class="p_25">
		            <input class="button edit" type="submit" value="Editar">
		            <input type="hidden" name="r" value="users-edit">
		            <input type="hidden" name="crud" value="set">
	            </div>
            </form>
		';

		printf(
			$template_users,
			$users[0]['user'],
			$users[0]['user'],
			$users[0]['email'],
			$users[0]['name'],
			$users[0]['birthday'],
			//$users[0]['pass'],
			$role_admin,
			$role_user
		);
	}

} else if ($_POST['r'] == 'users-edit' && $_SESSION['role'] == 'Admin' && $_POST['crud'] == 'set') {
	//echo '<h2>Estamos aca</h2>';
    $edit_users = array(
    	'user' => $_POST['user'],
    	'email' => $_POST['email'],
    	'name' => $_POST['name'],
    	'birthday' => $_POST['birthday'],
    	'pass' => $_POST['pass'],
    	'role' => $_POST['role'] 
    );

    $users = $users_controller->set($edit_users);

    $template = '
        <div class="container">
		    <p class="item edit">Usuario <b>%s</b> Editado</p>
	    </div>
	    <script>
	        window.onload = function (){
	        	reloadPage("usuarios")
	        }
	    </script>
    ';

    printf($template, $_POST['user']);
} else{ 
	$controller = new ViewController();
	$controller->load_view('error401');
}




