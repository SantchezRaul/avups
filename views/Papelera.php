                    
            <div class="p_25">
                <textarea name="plot" cols="22" row="10" placeholder="descripción"></textarea>
            </div>
    	    <div class="p_25">
                <input type="text" name="actors" placeholder="actores" required>
            </div>
            <div class="p_25">
                <input type="text" name="country" placeholder="país" required>
            </div>
            <div class="p_25">
                <input type="text" name="premiere" placeholder="estreno" required>
            </div>
    	    <div class="p_25">
    	        <input type="url" name="poster" placeholder="poster" required>
    	    </div>
    	    <div class="p_25">
    	        <input type="url" name="trailer" placeholder="trailer" required>
    	    </div>
            <div class="p_25">
                <input type="number" name="rating" placeholder="rating" required>
            </div>
            <div class="p_25">
                <input type="text" name="genres" placeholder="géneros" required>
            </div>
            <div class="p_25">
    	        <input type="radio" name="category" id="movie" value="Movie"  required>
                <label for="movie">Movie</label>
    	        <input type="radio" name="category" id="serie" value="Serie" required>
                <label for="serie">Serie</label>
    	    </div>