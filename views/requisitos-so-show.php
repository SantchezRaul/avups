<?php
if ( $_POST['r'] == 'requisitos-so-show' && isset($_POST['idSolicitud']) ) {
	
	$so_controller = new RegistroController();
	$so = $so_controller->get($_POST['idSolicitud']);

	if (empty($so)) {
		printf('
			<div class="container">
			    <p class="item error">Error al cargar el Registro</p>
			</div>
			', $_POST['idSolicitud']);
	} else {
		$template_so = '
		    <div class="item">
		    <form method="POST">
                <input type="hidden" name="r" value="requisitos-so-show">
                <input class="button edit" type="submit" value="Mostrar Requisitos">
            </form>
            <br></br>
			    <table>
				    <tr>
				        <th>Conceptos</th>
				        <th>Datos</th>
				    </tr>
				    <tr>
				    <p></p>
				    </tr>				    
				    <td>
					    <p>idSolicitud</p>
					    <p>fechaSolicitud</p>
					    <p>nombres</p>
						<p>apellidoUno</p>
						<p>apellidoDos</p>
						<p>apellidoCasada</p>
					    <p>fecha de Nacimiento</p>
					    <p>genero</p>
					    <p>dui</p>
					    <p>nit</p>
					    <p>carnetMinoridadoDos</p>
					    <p>idNacionalidad</p>
					    <p>idPaisesNacimiento</p>
						<p>direccion</p>
						<p>idPaisesDireccion</p>
						<p>idDepartamentoDireccion</p>
						<p>idMunicipioDireccion</p>
						<p>idCantonDireccion</p>
						<p>telefono</p>
						<p>correo</p>
						<p>correoDos</p>
						<p>idEstadoCivil</p>
						<p>idTipoIngreso</p>
						<p>idEstatusSolicitud</p>
						<p>usuarioIngreso</p>
						<p>fechaIngreso</p>
						<p>usuarioActualiza</p>
						<p>fechaActualiza</p>
					</td>
					<td>
					    <p class="block">%s</p>
					    <p class="block">%s</p>
					    <p class="block">%s</p>
					    <p class="block">%s</p>
					    <p class="block">%s</p>
					    <p class="block">%s</p>
					    <p class="block">%s</p>
					    <p class="block">%s</p>
					    <p class="block">%s</p>
					    <p class="block">%s</p>
					    <p class="block">%s</p>
					    <p class="block">%s</p>
					    <p class="block">%s</p>
					    <p class="block">%s</p>
					    <p class="block">%s</p>
					    <p class="block">%s</p>
					    <p class="block">%s</p>
					    <p class="block">%s</p>
					    <p class="block">%s</p>
					    <p class="block">%s</p>
					    <p class="block">%s</p>
					    <p class="block">%s</p>
					    <p class="block">%s</p>
					    <p class="block">%s</p>
					    <p class="block">%s</p>
					    <p class="block">%s</p>
					    <p class="block">%s</p>
					    <p class="block">%s</p>
					</td>
				</table>
		        <br></br>
		        <input class="p_5 button add" type="button" value="Regresar" onclick="history.back()">	        
		    </div>
		';

		printf(
            $template_so,
			$so[0]['idSolicitud'],
            $so[0]['fechaSolicitud'],
            $so[0]['nombres'],
            $so[0]['apellidoUno'],
            $so[0]['apellidoDos'],
            $so[0]['apellidoCasada'],
            $so[0]['fechaNacimiento'],
            $so[0]['genero'],
            $so[0]['dui'],
            $so[0]['nit'],
            $so[0]['carnetMinoridad'],
            $so[0]['idNacionalidad'],
			$so[0]['idPaisesNacimiento'],
            $so[0]['direccion'],
            $so[0]['idPaisesDireccion'],
            $so[0]['idDepartamentoDireccion'],
			$so[0]['idMunicipioDireccion'],
			$so[0]['idCantonDireccion'],
            $so[0]['telefono'],
            $so[0]['correo'],
            $so[0]['correoDos'],
            $so[0]['idEstadoCivil'],
			$so[0]['idTipoIngreso'],
			$so[0]['idEstatusSolicitud'],
            $so[0]['usuarioIngreso'],
            $so[0]['fechaIngreso'],
            $so[0]['usuarioActualiza'],
            $so[0]['fechaActualiza']                    
		);
	}
	
} else {
	$controller = new ViewController();
	$controller->load_view('error401');
}
