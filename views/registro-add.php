<?php
//echo '<h2>Guardar</h2>';

if ($_POST['r'] == 'registro-add' && $_SESSION['role'] == 'Admin' && !isset($_POST['crud']) ) {

    $nacio_controller = new NacionalidadController();
    $nacio = $nacio_controller->get();
    $nacio_select = '';

    for ($n=0; $n < count($nacio); $n++) { 
        $nacio_select .= '<option value="' . $nacio[$n]['idNacionalidad'] . '">' . $nacio[$n]['nombre'] . '</option>';
    }

    $ps_controller = new PaisController();
    $ps = $ps_controller->get();
    $ps_select = '';

    for ($n=0; $n < count($ps); $n++) { 
        $ps_select .= '<option value="' . $ps[$n]['idPaises'] . '">' . $ps[$n]['nombrePais'] . '</option>';
    }

    $dp_controller = new DeptoController();
    $dp = $dp_controller->get();
    $dp_select = '';

    for ($n=0; $n < count($dp); $n++) { 
        $dp_select .= '<option value="' . $dp[$n]['idDepartamento'] . '">' . $dp[$n]['nombreDepartamento'] . '</option>';
    }

    $mu_controller = new MuniController();
    $mu = $mu_controller->get();
    $mu_select = '';

    for ($n=0; $n < count($mu); $n++) { 
        $mu_select .= '<option value="' . $mu[$n]['idMunicipio'] . '">' . $mu[$n]['nombreMunicipio'] . '</option>';
    }

    $cn_controller = new CtonController();
    $cn = $cn_controller->get();
    $cn_select = '';

    for ($n=0; $n < count($cn); $n++) { 
        $cn_select .= '<option value="' . $cn[$n]['idCanton'] . '">' . $cn[$n]['nombreCanton'] . '</option>';
    }

    $ec_controller = new EsciController();
    $ec = $ec_controller->get();
    $ec_select = '';

    for ($n=0; $n < count($ec); $n++) { 
        $ec_select .= '<option value="' . $ec[$n]['idEstadoCivil'] . '">' . $ec[$n]['nombre'] . '</option>';
    }

    $ti_controller = new TinController();
    $ti = $ti_controller->get();
    $ti_select = '';

    for ($n=0; $n < count($ti); $n++) { 
        $ti_select .= '<option value="' . $ti[$n]['idTipoIngreso'] . '">' . $ti[$n]['nombre'] . '</option>';
    }

    $es_controller = new EsController();
    $es = $es_controller->get();
    $es_select = '';

    for ($n=0; $n < count($es); $n++) { 
        $es_select .= '<option value="' . $es[$n]['idEstatusSolicitud'] . '">' . $es[$n]['nombreEstatusSolicitud'] . '</option>';
    }
	
    printf('
    	<h2 class="p1">Agregar Registro</h2>
    	<form method="POST" class="item">
    	    <div class="p_25">
    	        <input type="text" name="idSolicitud" placeholder="idSolicitud" required>
    	    </div>
    	    <div class="p_25">
                <input type="text" name="fechaSolicitud" placeholder="fechaSolicitud" required>
            </div>
            <div class="p_25"><div class="p_25">
                <input type="text" name="nombres" placeholder="nombres" required>
            </div>
            <div class="p_25">
                <input type="text" name="apellidoUno" placeholder="apellidoUno" required>
            </div>
            <div>
                <input type="text" name="apellidoDos" placeholder="apellidoDos">
            </div>
            <div class="p_25">
                <input type="text" name="apellidoCasada" placeholder="apellidoCasada">
            </div>
            <div class="p_25">
                <input type="text" name="fechaNacimiento" placeholder="fechaNacimiento">
            </div>
            <div class="p_25">
                <input type="radio" name="genero" id="masculino" value="masculino"  required>
                <label for="masculino">Masculino</label>
                <input type="radio" name="genero" id="femenino" value="femenino" required>
                <label for="femenino">Femenino</label>
            </div>
            <div class="p_25">
                <input type="text" name="dui" placeholder="dui" required>
            </div>
            <div class="p_25">
                <input type="text" name="nit" placeholder="nit">
            </div>
            <div class="p_25">
                <input type="text" name="carnetMinoridad" placeholder="carnetMinoridad">
            </div>
            <div class="p_25">
                <select name="idNacionalidad" placeholder="nacionalidad" required>
                    <option value="">Nacionalidad</option>%s
                </select>
            </div>
            <div class="p_25">
                <select name="idPaisesNacimiento" placeholder="pais" required>
                    <option value="">Pais de Nacimiento</option>%s
                </select>
            </div>
            <div class="p_25">
                <input type="text" name="direccion" placeholder="Dirección">
            </div>
            <div class="p_25">
                <select name="idPaisesDireccion" placeholder="pais" required>
                    <option value="">Pais Actual</option>%s
                </select>
            </div>
            <div class="p_25">
                <select name="idDepartamentoDireccion" placeholder="departamento" required>
                    <option value="">Departamento</option>%s
                </select>
            </div>
            <div class="p_25">
                <select name="idMunicipioDireccion" placeholder="municipio" required>
                    <option value="">Municipios</option>%s
                </select>
            </div>
            <div class="p_25">
                <select name="idCantonDireccion" placeholder="canton" required>
                    <option value="">Canton</option>%s
                </select>
            </div>
            <div class="p_25">
                <input type="text" name="telefono" placeholder="telefono">
            </div>
            <div class="p_25">
                <input type="text" name="correo" placeholder="correo">
            </div>
            <div class="p_25">
                <input type="text" name="correoDos" placeholder="correoDos">
            </div>
            <div class="p_25">
                <select name="idEstadoCivil" placeholder="estado civil" required>
                    <option value="">Estado Civil</option>%s
                </select>
            </div>
            <div class="p_25">
                <select name="idTipoIngreso" placeholder="tipo de ingreso" required>
                    <option value="">Tipo de ingreso</option>%s
                </select>
            </div>
            <div class="p_25">
                <select name="idEstatusSolicitud" placeholder="estado de solicitud" required>
                    <option value="">Estado de solicitud</option>%s
                </select>
            </div>
            <div class="p_25">
                <input type="text" name="usuarioIngreso" placeholder="usuarioIngreso">
            </div>
            <div class="p_25">
                <input type="text" name="fechaIngreso" placeholder="fechaIngresa">
            </div>
            <div class="p_25">
                <input type="text" name="usuarioActualiza" placeholder="usuarioActualiza">
            </div>
            <div class="p_25">
                <input type="text" name="fechaActualiza" placeholder="fechaActualiza">
            </div>
	        <div class="p_25">
		        <input class="button add"  type="submit" name="r" value="Agregar">
		        <input type="hidden" name="r" value="registro-add">
		        <input type="hidden" name="crud" value="set">
	        </div>
        </form>
    ', $nacio_select, $ps_select, $ps_select, $dp_select, $mu_select, $cn_select, $ec_select, $ti_select, $es_select);

} else if ($_POST['r'] == 'registro-add' && $_SESSION['role'] == 'Admin' && $_POST['crud'] == 'set') {
	$so_controller = new RegistroController();

    $new_so = array(    	
        'idSolicitud' =>$_POST['idSolicitud'],
        'fechaSolicitud' =>$_POST['fechaSolicitud'],
        'nombres' =>$_POST['nombres'],
        'apellidoUno' =>$_POST['apellidoUno'],
        'apellidoDos' =>$_POST['apellidoDos'],
        'apellidoCasada' =>$_POST['apellidoCasada'],
        'fechaNacimiento' =>$_POST['fechaNacimiento'],
        'genero' =>$_POST['genero'],
        'dui' =>$_POST['dui'],
        'nit' =>$_POST['nit'],
        'carnetMinoridad' =>$_POST['carnetMinoridad'],
        'idNacionalidad' =>$_POST['idNacionalidad'],
        'idPaisesNacimiento' =>$_POST['idPaisesNacimiento'],
        'direccion' =>$_POST['direccion'],
        'idPaisesDireccion' =>$_POST['idPaisesDireccion'],
        'idDepartamentoDireccion' =>$_POST['idDepartamentoDireccion'],
        'idMunicipioDireccion' =>$_POST['idMunicipioDireccion'],
        'idCantonDireccion' =>$_POST['idCantonDireccion'], 
        'telefono' =>$_POST['telefono'],
        'correo' =>$_POST['correo'],
        'correoDos' =>$_POST['correoDos'],
        'idEstadoCivil' =>$_POST['idEstadoCivil'],
        'idTipoIngreso' =>$_POST['idTipoIngreso'],
        'idEstatusSolicitud' =>$_POST['idEstatusSolicitud'],
        'usuarioIngreso' =>$_POST['usuarioIngreso'],
        'fechaIngreso' =>$_POST['fechaIngreso'],
        'usuarioActualiza' =>$_POST['usuarioActualiza'],
        'fechaActualiza' =>$_POST['fechaActualiza']
    );

    $so = $so_controller->set($new_so);

    $template = '
        <div class="container">
		    <p class="item add">Registro <b>%s</b> Salvado </p>
	    </div>
	    <script>
	        window.onload = function (){
	        	reloadPage("registro")
	        }
	    </script>
    ';

    printf($template, $_POST['idSolicitud']);
} else{ 
	$controller = new ViewController();
	$controller->load_view('error401');
}


