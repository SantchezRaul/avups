<?php
print('<h2 class="p1">GESTIÓN DE REGISTROS</h2>');

$so_controller = new RegistroController();
$so = $so_controller->get();

if ( empty($so) ) {
	print('
		<div class="container">
		    <p class="item error">No hay Registros</p>
		</div>
	');
} else {
	$template_so = '
	    <div class="item">
            <table>
	            <tr>
		            <th>IdSolicitud</th>
		            <th>FechaSolicitud</th>
		            <th>Nombres</th>
		            <th>ApellidoUno</th>
		            <th>Dui</th>
		            <th>IdNacionalidad</th>
		            <th colspan="3">
		                <form method="POST">
		                    <input type="hidden" name="r" value="registro-add">
		                    <input class="button add" type="submit" value="Agregar">
		                </form>
		            </th>
	            </tr>';

	   for ($n=0; $n < count($so); $n++) { 
	    	$template_so .= '
	    		<tr>
	    		    <td>'. $so[$n]['idSolicitud'] .'</td>
	    		    <td>'. $so[$n]['fechaSolicitud'] .'</td>
	    		    <td>'. $so[$n]['nombres'] .'</td>
	    		    <td>'. $so[$n]['apellidoUno'] .'</td>
	    		    <td>'. $so[$n]['dui'] .'</td>
	    		    <td>'. $so[$n]['idNacionalidad'] .'</td>
	    		    <td>
	    		        <form method="POST">
		                    <input type="hidden" name="r" value="registro-show">
		                    <input type="hidden" name="idSolicitud" value="'. $so[$n]['idSolicitud'] .'">
		                    <input class="button show" type="submit" value="mostrar">
		                </form>
		            </td>
	    		    <td>
	    		        <form method="POST">
		                    <input type="hidden" name="r" value="registro-edit">
		                    <input type="hidden" name="idSolicitud" value="'. $so[$n]['idSolicitud'] .'">
		                    <input class="button edit" type="submit" value="Editar">
		                </form>
		            </td>
	    		    <td>
	    		        <form method="POST">
		                    <input type="hidden" name="r" value="registro-delete">
		                    <input type="hidden" name="idSolicitud" value="'. $so[$n]['idSolicitud'] .'">
		                    <input class="button delete" type="submit" value="Eliminar">
		                </form>
		            </td>
		        </tr>
		    ';
	}

	$template_so .='        
       	    </table>
	    </div>
	';

    print($template_so);
}