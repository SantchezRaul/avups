<?php
//echo '<h2>Eliminar</h2>';
$users_controller = new UsersController();

if ($_POST['r'] == 'users-delete' && $_SESSION['role'] == 'Admin' && !isset($_POST['crud']) ) {
	
    $users = $users_controller->get($_POST['user']);

    if ( empty($users) ) {
    	$template = '
            <div class="container">
		        <p class="item error">No existe el user <b>%s</b></p>
	        </div>
	        <script>
	            window.onload = function (){
	            	reloadPage("usuarios")
	            }
	        </script>
        ';

        printf($template, $_POST['user']);
    } else {
    	$template_users = '
            <h2 class="p1">Eliminar Usuarios</h2>
            <form method="POST" class="item">
	            <div class="p1 f2">
		            Estas seguro de Eliminar el Usuario: <mark class="p1">%s</mark>?
	            </div>	             
	            <div class="p_25">
		            <input class="button delete" type="submit" value="SI">
		            <input class="button add" type="button" value="NO" onclick="history.back()">
		            <input type="hidden" name="user" value="%s">
	                <input type="hidden" name="r" value="users-delete">
	                <input type="hidden" name="crud" value="del">
	            </div>
            </form>
    	';

    	printf(
    		$template_users,
    		$users[0]['user'],
    		$users[0]['user']    		
    	);
    }    

} else if ($_POST['r'] == 'users-delete' && $_SESSION['role'] == 'Admin' && $_POST['crud'] == 'del') {
	
    $users = $users_controller->del($_POST['user']);

    $template = '
        <div class="container">
		   <p class="item edit">Usuario <b>%s</b> Eliminado </p>
	    </div>
	    <script>
	        window.onload = function (){
	        	reloadPage("usuarios")
	        }
	    </script>
    ';

    printf($template, $_POST['user']);
} else{ 
	$controller = new ViewController();
	$controller->load_view('error401');
}

