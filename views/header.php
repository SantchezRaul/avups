<?php
print ('
<!DOCTYPE html>
<html lang="es">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="utf-8">
	<title>Inscripción UPS</title>
	<meta name="description" content="Inscripción en línea ups">
	<link rel="schortcut icon" type="image/png" href="./public/img/favicon.png">
	<link rel="stylesheet" type="text/css" href="./public/css/responsimple.min.css">
	<link rel="stylesheet" type="text/css" href="./public/css/avups.css">
</head>
<body>
	<header class="container center header">
		<div class="item i-b v-middle ph12 lg2">
			<h1 class="logo">PROTO</h1>
		</div>
');
if ($_SESSION['ok']) {
	print('
		<nav class="item i-b v-middle ph12 lg10 menu">
			<ul>
				<li class="nobullet item inline"><a href="./">Inicio</a></li>
				<li class="nobullet item inline"><a href="usuarios">Usuarios</a></li>
				<li class="nobullet item inline"><a href="registro">Registro</a></li>
				<li class="nobullet item inline"><a href="salir">Salir</a></li>		
			</ul>
		</nav>
	');
}
		
print ('				
	</header>
	<main class="container center header">
');