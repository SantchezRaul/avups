<?php
//echo '<h2>Eliminar</h2>';
$so_controller = new RegistroController();

if ($_POST['r'] == 'registro-delete' && $_SESSION['role'] == 'Admin' && !isset($_POST['crud']) ) {
	
    $so = $so_controller->get($_POST['idSolicitud']);

    if ( empty($so) ) {
    	$template = '
            <div class="container">
		        <p class="item error">No existe el Registro <b>%s</b></p>
	        </div>
	        <script>
	            window.onload = function (){
	            	reloadPage("registro")
	            }
	        </script>
        ';

        printf($template, $_POST['idSolicitud']);
    } else {
    	$template_so = '
            <h2 class="p1">Eliminar Registro</h2>
            <form method="POST" class="item">
	            <div class="p1 f2">
		            Estas seguro de Eliminar el Registro: <mark class="p1">%s</mark>?
	            </div>	             
	            <div class="p_25">
		            <input class="button delete" type="submit" value="SI">
		            <input class="button add" type="button" value="NO" onclick="history.back()">
		            <input type="hidden" name="idSolicitud" value="%s">
	                <input type="hidden" name="r" value="registro-delete">
	                <input type="hidden" name="crud" value="del">
	            </div>
            </form>
    	';

    	printf(
    		$template_so,
    		$so[0]['idSolicitud'],
    		$so[0]['idSolicitud']    		
    	);
    }    

} else if ($_POST['r'] == 'registro-delete' && $_SESSION['role'] == 'Admin' && $_POST['crud'] == 'del') {
	
    $so = $so_controller->del($_POST['idSolicitud']);

    $template = '
        <div class="container">
		   <p class="item error">Registro <b>%s</b> Eliminado </p>
	    </div>
	    <script>
	        window.onload = function (){
	        	reloadPage("registro")
	        }
	    </script>
    ';

    printf($template, $_POST['idSolicitud']);
} else{ 
	$controller = new ViewController();
	$controller->load_view('error401');
}

