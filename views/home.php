<?php
$template = '
<article class="item">
    <h2>Hola %s, bienvenid@ Inscripción en línea avups</h2>
    <p>Tu Nombre es <b>%s</b></p>
    <p>Tu perfil de usuario tiene un nivel de <b>%s</b></p>
</article>
';

printf(
    $template,
    $_SESSION['user'], 
    //$_SESSION['email'], 
    $_SESSION['name'], 
    //$_SESSION['birthday'],
    $_SESSION['role']
);
