<?php
//echo '<h2>Guardar</h2>';

if ($_POST['r'] == 'users-add' && $_SESSION['role'] == 'Admin' && !isset($_POST['crud']) ) {
	print('
    	<h2 class="p1">Agregar Usuarios</h2>
    	<form method="POST" class="item">
    	    <div class="p_25">
    	        <input type="text" name="user" placeholder="usuario" required>
    	    </div>
    	    <div class="p_25">
    	        <input type="email" name="email" placeholder="email" required>
    	    </div>
    	    <div class="p_25">
    	        <input type="text" name="name" placeholder="name" required>
    	    </div>
    	    <div class="p_25">
    	        <input type="text" name="birthday" placeholder="cumpleanos" required>
    	    </div>
    	    <div class="p_25">
    	        <input type="password" name="pass" placeholder="password" required>
    	    </div>
    	    <div class="p_25">
    	        <input type="radio" name="role" id="admin" value="Admin"  required><label for="admin">Administrador</label>
    	        <input type="radio" name="role" id="noadmin" value="User" required><label for="noadmin">Usuario</label>
    	    </div>
	        <div class="p_25">
		        <input class="button add"  type="submit" name="r" value="Agregar">
		        <input type="hidden" name="r" value="users-add">
		        <input type="hidden" name="crud" value="set">
	        </div>
        </form>
    ');

} else if ($_POST['r'] == 'users-add' && $_SESSION['role'] == 'Admin' && $_POST['crud'] == 'set') {
	$users_controller = new UsersController();

    $new_users = array(
    	'user' => $_POST['user'],
    	'email' => $_POST['email'],
    	'name' => $_POST['name'],
    	'birthday' => $_POST['birthday'],
    	'pass' => $_POST['pass'],
    	'role' => $_POST['role']
    );

    $users = $users_controller->set($new_users);

    $template = '
        <div class="container">
		    <p class="item add">Usuario <b>%s</b> Salvado </p>
	    </div>
	    <script>
	        window.onload = function (){
	        	reloadPage("usuarios")
	        }
	    </script>
    ';

    printf($template, $_POST['user']);
} else{ 
	$controller = new ViewController();
	$controller->load_view('error401');
}


